var gulp           = require('gulp'),
	sass           = require('gulp-sass'),
	autoprefixer   = require('gulp-autoprefixer');




gulp.task('sass', function(){
	return gulp.src('app/sass/style.scss')  
        .pipe(sass()) 
        .pipe(gulp.dest('app/css')) 
});

gulp.task('autoprefixer', function(){
    gulp.src('app/css/*.css')
        .pipe(autoprefixer({
            browsers: ['last 30 versions'],
            cascade: true
        }))
        .pipe(gulp.dest('app/css'))
});


gulp.task('watch', function() {
    gulp.watch('app/css/*.css', ['autoprefixer']),
	gulp.watch('app/sass/*.scss', ['sass']);
});