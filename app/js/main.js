$( document ).ready(function() {
    $(".link_menu").each(function(i){
		$(this).attr("href", "#tab" + i);							   
	});
	$(".wrapp").each(function(i){
		$(this).attr("id", "tab" + i);							   
	});
	$(".tab_menu li").click(function(e){
		e.preventDefault();		
		$(".tab_menu .active").removeClass("active");
		$(this).addClass("active");
		var tab = $(this).find("a").attr("href");
		$(".wrapp").not(tab).css({"display":"none"});
		$(tab).fadeIn(1000);
	});
	
	$("#js_btn1").click(function(){
		$(".drop_list1").slideToggle();	
	});
	
	$("#js_btn2").click(function(){
		$(".drop_list2").slideToggle();	
	});
	
	$("#js_btn3").click(function(){
		$(".drop_list3").slideToggle();	
	});
	
	$("#js_btn4").click(function(){
		$(".drop_list4").slideToggle();	
	});
	
	$("#js_btn5").click(function(){
		$(".drop_list5").slideToggle();	
	});
	
	$("#js_btn6").click(function(){
		$(".drop_list6").slideToggle();	
	});
	
	$(".first").click(function(){
        $(".ajax").load("../test.html",function() {
  			alert( "Load was performed." );
		});
	});
});